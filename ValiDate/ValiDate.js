/*ASCII
45 - 57 = Numbers
96 - 105 = NumberPad
13 = TAB
8 = Backspace
*/

function ValiDate(_input, _separator) {
  
    $("#" + _input).attr("maxlength", "10");

    $("#" + _input).click(function (e) {

        lon = $("#" + _input).val().length;
        txt = e.currentTarget;

        if (e.currentTarget.selectionStart < lon)
            $("#" + _input).val($("#" + _input).val().substring(0, e.currentTarget.selectionStart))
    });

    function daysInMonth(year, month) {
        return new Date(year, month, 0).getDate();
    }
    var text = "";
    var bkpc = 8;

    $("#" + _input).keydown((e) => {

        var key = window.Event ? e.which : e.keyCode
        var keyValue = e.key;

        if (key >= 48 && key <= 57 || key == 13 || key == 8 || key == 9 || key >= 96 && key <= 105) {

            text = $("#" + _input).val();
            var tLen = text.length;
            var code = e.keyCode;

            //First number
            if (tLen == 0) {
                if (keyValue !== "2" && keyValue !== "1")
                    return false;
            }

            //Adding the separator
            if (tLen === 4) {
                if (code !== bkpc) {
                    if (keyValue !== "1" && keyValue !== "Backspace" && keyValue !== "0")
                        return false;
                    else text += _separator;
                }
            }

            //Deleting separator and validating month
            if (tLen === 6) {
                var tsplit = text.split(_separator);

                if (code === bkpc) {
                    if (tLen === 6)
                        text = text.substring(0, tLen - 1);
                } else {
                    if (tsplit[1] === "1") {
                        if (parseInt(keyValue) > 2)
                            return false;
                    } else if (tsplit[1] === "0") {
                        if (parseInt(keyValue) < 1)
                            return false;
                    }
                }
            }

            //Adding and validating days
            if (tLen === 7) {
                if (code !== bkpc) {

                    var tsplit = text.split(_separator);
                    var days = daysInMonth(parseInt(tsplit[0]), parseInt(tsplit[1]));
                    var dsplit = days.toString().split("");

                    switch (dsplit[0]) {
                        case "3":
                            if (keyValue !== "0" && keyValue !== "1" && keyValue !== "2" && keyValue !== "3")
                                return false;
                            else text += _separator;
                            break;

                        case "2":
                            if (keyValue !== "0" && keyValue !== "1" && keyValue !== "2")
                                return false;
                            else text += _separator;
                            break;
                    }
                }
            }

            //Deleting separator and validating month
            if (tLen === 9) {
                var tsplit = text.split(_separator);
                var days = daysInMonth(parseInt(tsplit[0]), parseInt(tsplit[1]));
                var dsplit = days.toString().split("");

                if (code === bkpc) {
                    if (tLen === 9)
                        text = text.substring(0, tLen - 1);
                } else {
                    if (tsplit[2] === "0") {
                        if (parseInt(keyValue) < 1)
                            return false;
                    }
                    switch (dsplit[0]) {
                        case "3":
                            if (tsplit[2] === "3") {
                                if (dsplit[1] === "0") {
                                    if (keyValue !== "0")
                                        return false;
                                } else if (dsplit[1] === "1") {
                                    if (keyValue !== "1" && keyValue !== "0")
                                        return false;
                                }
                            }
                            break;

                        case "2":
                            if (parseInt(keyValue) > parseInt(dsplit[1]))
                                return false;
                            break;             
                    }
                }
            }

            $("#" + _input).val(text);

        } else return false;
    })
}
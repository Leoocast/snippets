# ValiDate

Helps to validate the date on an input. It puts automatically an `/` or any char or word you give it.

## Content

* ValiDate.js 

## Usage

```javascript

ValiDate("inputID","/"); //Returns 2018/08/22

ValiDate("inputID","-"); //Returns 2018-08-22

ValiDate("inputID","_"); //Returns 2018_08_22

```